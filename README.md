# Extended Personal Media Scanner

This scanner is meant to be used with the Extended Media Metadata Agent. See the documentation on this [page](https://bitbucket.org/mjarends/extendedpersonalmedia-agent.bundle) on the metadata agent plugin to see how files should be organized within your personal media sections.

This scanner is not meant to be full replacement of the Plex Media Scanner. Requests for functionality will be considered but may be limited by what Plex currently allows in the TV Show sections.

## Change Log

[Change Log](https://forums.plex.tv/discussion/88982/rel-extended-personal-media-scanner/p1)

## Differences between this scanner and Plex Media Scanner

This scanner differentiates itself from the Plex Media Scanner by:

* **Uniquely assigns an episode number to date-based personal media.** This allows for multiple shows on the same date. This is particularly useful for home movies, organizing sports videos, etc.
* This scanner **does not read tag data from MP4 files** or other types of media. This may be added in a future release if there is enough demand for it.

## Known Issues

### Issue

* Renaming shows that have already been processed sometimes causes Plex to create a new TV show within Plex.

### How to fix

1. Remove the files that were created in the new TV show from the original directory.
2. In Plex, scan the section so that Plex detects that the files have been removed.
3. Re-add the files back to the original directory.
4. In Plex, scan the section so that Plex detects the files and adds them back to the original TV Show.

## Download and source

[Download](https://bitbucket.org/mjarends/plex-scanners/get/master.zip)

[Source](https://bitbucket.org/mjarends/plex-scanners/src)

## Installation

### Instructions

1. Unzip the downloaded ZIP file, this gives you a file/folder with the name plex-scanners-XXXXXXXX. Rename the folder to plex-scanners.
2. Copy the Series folder from the plex-scanners folder created above to the Scanners folder within the Plex installation directory. See the steps below for OS specific steps.
3. Follow the instructions in the Mac, Windows, or Linux sections below to locate the `Scanners` folder on your system and install the scanner.
4. Install the Extended Personal Media Metadata Agent plugin. Use the instructions [here](https://bitbucket.org/mjarends/extendedpersonalmedia-agent.bundle/src/master/README.md).

### Mac specific instructions

* Copy the Series folder from the plex-scanners folder created above to ~/Library/Application Support/Plex Media Server/Scanners folder.
  * The easiest way to find this folder is to use the Go to folder... option in the Go menu of the Finder.*
  * ~ is your home folder. If you can't find your Library folder, have a look at [OS X Lion: Where did my Library go?](http://reviews.cnet.com/8301-13727_7-20082044-263/os-x-lion-where-did-my-library-go/)*

### Windows specific instructions

* Copy the Series folder from the plex-scanners folder created above to the Plex Media Server Scanners directory.
* Open File Explorer and navigate to `C:\Users\<User Name>\AppData\Local\Plex Media Server\Plug-ins` (if you moved your Plex user data directory then go to `<Plex User Data Directory>\Plex Media Server\Plug-ins`)
* If the `Series` folder does exists in the `Scanners` directory, create a new folder with the name `Scanners` and place the Series folder in it.

### Linux (Ubuntu) specific instructions

* Create the Scanners directory

```shell
cd /var/lib/plexmediaserver/Library/Application Support/Plex Media Server

sudo mkdir Scanners

sudo cp -R [directory a] [directory b]

sudo chown -R plex:plex Scanners [making sure the person is still in /var/lib/plexmediaserver/Library/Application Support/Plex Media Server]
```

* Copy the Series folder from the plex-scanners folder created above to /var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Scanners
